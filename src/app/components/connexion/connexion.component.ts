import { Component } from '@angular/core';
import { AuthentificationService } from '../../services/authentification.service';


@Component({
    selector: 'app-connexion',
    templateUrl: './connexion.component.html',
    styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent {
    email: string = '';
    password: string = '';
    verificationCode: string = '';
    isLoginSuccessful: boolean = false;

    constructor(private authService: AuthentificationService) { }

    login(): void {
        this.authService.login(this.email, this.password).subscribe(
            response => {
                this.isLoginSuccessful = true;
            },
            error => {
                console.error(error);
                alert('Erreur lors de l\'authentification.');
            }
        );
    }

    verifyCode(): void {
        this.authService.verifyCode(this.email, this.verificationCode).subscribe(
            response => {
                alert('Authentification à deux facteurs réussie.');
            },
            error => {
                console.error(error);
                alert('Erreur lors de l\'authentification à deux facteurs.');
            }
        );
    }
}
