import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnexionComponent } from './components/connexion/connexion.component';
import { AuthenticatorComponent } from './components/authenticator/authenticator.component';

const routes: Routes = [
    { path: '', component: ConnexionComponent },
    { path: "authentication", component: AuthenticatorComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
