import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthentificationService {
    private baseUrl = 'http://localhost:3001';

    constructor(private http: HttpClient) { }

    login(email: string, password: string): Observable<any> {
        const body = { email, password };
        return this.http.post(`${this.baseUrl}/login`, body);
    }

    verifyCode(email: string, verificationCode: string): Observable<any> {
        const body = { email, verificationCode };
        return this.http.post(`${this.baseUrl}/verify-code`, body);
    }
}
